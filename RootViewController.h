//
//  RootViewController.h
//  Journal
//
//  Created by Mark Farrell on 2/2/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

{
    UITableView* myTableView;
    NSMutableArray* array;
    
}

@end
