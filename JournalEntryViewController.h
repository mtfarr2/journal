//
//  JournalEntryViewController.h
//  Journal
//
//  Created by Mark Farrell on 2/2/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface JournalEntryViewController : UIViewController
{
    UITextField* title;
    UITextView* tv;
    BOOL newEntry;
    
}

@property (nonatomic, strong) NSMutableDictionary* journalDictionaryEntry;
@property (nonatomic) NSUInteger curIndex;
@end
