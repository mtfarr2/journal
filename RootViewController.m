//
//  RootViewController.m
//  Journal
//
//  Created by Mark Farrell on 2/2/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "RootViewController.h"
#import "AppDelegate.h"
#import "JournalEntryViewController.h"


@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    UIBarButtonItem* addNewEntry = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewEntryTouched:)];
    NSArray *actionBtns = @[addNewEntry];
    self.navigationItem.rightBarButtonItems = actionBtns;
    
    myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 20) style:UITableViewStylePlain];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    [self.view addSubview:myTableView];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self loadData];
}

-(void)loadData{
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    array = ad.journalData;
    
    [myTableView reloadData];
}

-(void)addNewEntryTouched:(id)sender{
    JournalEntryViewController* jevc = [JournalEntryViewController new];
    [self.navigationController pushViewController:jevc animated:YES];
    
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
    return array.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    JournalEntryViewController *jevc = [JournalEntryViewController new];
    jevc.journalDictionaryEntry = [array objectAtIndex:indexPath.row];
    
    jevc.curIndex = indexPath.row;
    
    [self.navigationController pushViewController:jevc animated:YES];
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    NSString* identifier=@"cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    NSDictionary* dictionary = [array objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [dictionary objectForKey:@"title"];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
