//
//  JournalEntryViewController.m
//  Journal
//
//  Created by Mark Farrell on 2/2/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "JournalEntryViewController.h"

@interface JournalEntryViewController ()

@end

@implementation JournalEntryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor lightGrayColor]];
    UIBarButtonItem* saveBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(btnTouched:)];
    
    NSArray *actionButtonItems = @[saveBtn];
    self.navigationItem.rightBarButtonItems = actionButtonItems;
    
    title = [[UITextField alloc]initWithFrame:CGRectMake(20, 80, self.view.frame.size.width - 40, 40)];
    title.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:title];
    
    tv = [[UITextView alloc]initWithFrame:CGRectMake(20, 130, self.view.frame.size.width - 40, 270)];
    tv.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:tv];
    
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"Done Editing" forState:UIControlStateNormal];
    btn.frame = CGRectMake(0, 0, self.view.frame.size.width, 50);
    [btn addTarget:self action:@selector(doneEditing:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor greenColor]];
    tv.inputAccessoryView = btn;
    
    
    
    if (!self.journalDictionaryEntry) {
        self.journalDictionaryEntry = [[NSMutableDictionary alloc]init];
        newEntry = YES;
    } else if (self.journalDictionaryEntry){
        newEntry = NO;
        title.text = [self.journalDictionaryEntry objectForKey:@"title"];
        tv.text = [self.journalDictionaryEntry objectForKey:@"entryText"];
    }

}

-(void)doneEditing:(id)sender{
    [self.view endEditing:YES];
    
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [self.journalDictionaryEntry setObject:title.text forKey:@"title"];
    //[ad.journalData addObject:self.journalDictionaryEntry];
    
    [self.journalDictionaryEntry setObject:tv.text forKey:@"entryText"];
    if (newEntry) {
        [ad.journalData addObject:self.journalDictionaryEntry];
    }else if (!newEntry){
        [ad.journalData replaceObjectAtIndex:self.curIndex withObject:self.journalDictionaryEntry];
    }
    
}

-(void)btnTouched:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [self.journalDictionaryEntry setObject:title.text forKey:@"title"];
    //[ad.journalData addObject:self.journalDictionaryEntry];
    
    [self.journalDictionaryEntry setObject:tv.text forKey:@"entryText"];
    if (newEntry) {
        [ad.journalData addObject:self.journalDictionaryEntry];
    }else if (!newEntry){
        [ad.journalData replaceObjectAtIndex:self.curIndex withObject:self.journalDictionaryEntry];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
